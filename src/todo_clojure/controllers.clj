(ns todo-clojure.controllers
	(:use [compojure.core :only [defroutes GET POST]])
	(:use [ring.util.response :only [response status redirect-after-post]])
	(:require [todo-clojure.models :as models]))

(defn- switch-keys [oldkey newkey themap]
	(zipmap (map #(if (= oldkey %) newkey % ) (keys themap)) (vals themap)))

(defn task-list []
	(let [task-list (models/task-list)]
		(response (map #(switch-keys :_id :id %) task-list)))) 

(defn task-completed [id]
	(models/task-completed id)
	(redirect-after-post "/api/1/tasklist"))

(defn add-task [description]
	(models/add-task description)
	(redirect-after-post "/api/1/tasklist"))

(defroutes routes
  (GET   "/api/1/tasklist" [] (task-list))
  (POST  "/api/1/tasklist/:id/done" [id] (task-completed id))
  (POST  "/api/1/tasklist" {{description :task} :params} (add-task description)))
