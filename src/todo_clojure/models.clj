(ns todo-clojure.models
  (:use somnium.congomongo)
  (:require [todo-clojure.config :as config]))

;;Connect to the mongodb server

(defn connect []
  (let [{user :username pwd :password host :host port :port db :database} config/mongodb
        conn (make-connection db :host host :port port)]
    (authenticate conn user pwd)
    conn))

(defn task-list []
  (with-mongo (connect)
    (fetch :tasks)))

(defn task-completed [id]
  (with-mongo (connect)
    (destroy! :tasks {:_id (org.bson.types.ObjectId. id)})))

(defn add-task [description]
  (with-mongo (connect)
    (insert! :tasks {:task description})))