(ns todo-clojure.config)

(defn env [name & default]
	(or (System/getenv name) (first default)))

(defn parse-uri [uri]
	(let [[all user password host port database ] 
		(re-matches #"mongodb://(.*):(.*)@(.*):(.*)/(.*)" uri)]
		{:username user :password password :host host :port (Integer/parseInt port) 
			:database database}))

(def port (Integer/parseInt (env "PORT" "3000")))


(def mongodb (parse-uri (env "MONGODB_URI" "mongodb://user:pwd@server:5/db")))