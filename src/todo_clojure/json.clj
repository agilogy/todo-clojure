(ns todo-clojure.json
	(:use [clj-json.core])
	(:use [clojure.contrib.string :only [substring?]]))

(defn response-body-to-json [response]
	(binding [clj-json.core/*coercions* {org.bson.types.ObjectId (fn [x] (str x))}] 
		(assoc response :body (generate-string (:body response)))))

(defn json-middleware [app]
  (fn [req]
  	(let [accept (get (:headers req) "accept" "notspecified")
  			response (app req)]
    	(cond 
	    	(substring? "application/json" accept) 
	    		(response-body-to-json response)
        	:else response ))))

