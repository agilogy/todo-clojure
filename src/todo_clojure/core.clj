(ns todo-clojure.core
	(:use ring.adapter.jetty)
	(:use [compojure.core :only [defroutes]])
	(:require 	[todo-clojure.config :as config]
				[todo-clojure.controllers :as controllers]
				[todo-clojure.json :as json]
				[compojure.handler :as handler]
				[compojure.route :as route]))

(defroutes main-routes
   controllers/routes
   (route/resources "/" {:root "www"})
   (route/not-found "Page not found"))
 
(def app 
	(-> (handler/api main-routes)
      	(json/json-middleware)))

(defn -main []
    (run-jetty app {:port config/port}))