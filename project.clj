(defproject todo-clojure "1.0.0-SNAPSHOT"
  :description "TO-DO application written in clojure and jquery mobile"
  :dependencies [[org.clojure/clojure "1.2.1"]
  				[org.clojure/clojure-contrib "1.2.0"]
  				[ring/ring-jetty-adapter "1.0.0"]
  				[compojure "0.6.4"]
  				[midje "1.3.0"]
          [clj-json "0.4.3"]
          [congomongo "0.1.7"]]
  :dev-dependencies [[lein-ring "0.4.5"]
  					 [lein-midje "1.0.7"]]
  :ring {:handler todo-clojure.core/app} 
  :main todo-clojure.core)