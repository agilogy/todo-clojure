var get = function(uri, success) {
	$.ajax({
		type: 'get',
		url: '/api/1' + uri,
		dataType: "json",
		success: success
	});
}

var post= function(data, uri, success) {
	$.ajax({
		type: 'post',
		url: '/api/1' + uri,
		dataType: "json",
		data:data,
		success: success
	});
}

//Everything is under the app object
var app = {};
app.models = {
	updateTaskList: function(tasks) {
		app.models.tasklist=tasks;
		$(app.models).trigger('updated');
	},
	completeTask: function(task) {
		post({}, '/tasklist/'+task.id+'/done', function(tasks) {
			app.models.updateTaskList(tasks);
		})
	},
	addTask: function(task) {
		post({task: task}, '/tasklist', function(tasks){
			app.models.updateTaskList(tasks);			
		});
	},
	init: function() {
		get('/tasklist', function(tasks) {
			app.models.updateTaskList(tasks);
		});
	},
	tasklist: []
}

app.controllers = {
	taskCompleted: function(task) {
		app.models.completeTask(task);
		$.mobile.changePage('#taskListView');
	}, 
	init: function() {
		console.log($('[data-rel=save]','#addTaskView'));
		$('[data-rel=save]','#addTaskView').click(function(){
			app.models.addTask($('#task').val());
			$('#task').val('');
			$.mobile.changePage('#taskListView');
		});
		$(app.models).bind('updated', function() {
			if($.mobile.activePage.attr('id')=='taskListView') {
				app.views.taskListView.refresh();
			}
		});
		$('#taskListView').live('pagebeforeshow', function(event){
			app.views.taskListView.refresh();
		})
	}
}
app.views = {
	taskListView: {
		refresh: function() {
			var listView = 	$("[data-role=listview]", "#taskListView");
			listView.html("");
			$(app.models.tasklist).each(function(i, task) {
				var cell = $("<li>");
				var checkboxId='mark-done-'+task.id;
				var checkbox = $('<input>').attr('type', 'checkbox')
								.attr('id',checkboxId).attr('class','custom');
				checkbox.click(function() {
					app.controllers.taskCompleted(task);
				});
				checkbox.appendTo(cell);
				var label=$("<label>").attr('for',checkboxId).html(task.task);
				label.appendTo(cell);

				listView.append(cell);
			});
			listView.listview('refresh');
		}
	},
	init: function() {}
}
//$(document).bind("mobileinit", function(){
$(document).ready(function(){
  	app.views.init();
	app.models.init();
	app.controllers.init();
})