(ns todo-clojure.test.models
  (:use [midje.sweet])
  (:use [todo-clojure.models])
  (:require [somnium.congomongo :as mongo]))

(def connection {:db :whatever :mongo :whatever})

(fact (task-list) => :whatever
  (provided (connect) => connection)
  (provided (mongo/fetch :tasks) => :whatever :times 1))

(def task-id "4ec4431e0364a810ff60793e")

(fact (task-completed task-id) => anything
  (provided (connect) => connection)
  (provided (mongo/destroy! :tasks {:_id (org.bson.types.ObjectId. task-id)}) => :whatever :times 1))

(def task-desc "do something")

(fact (add-task task-desc) => anything
  (provided (connect) => connection)
  (provided (mongo/insert! :tasks {:task task-desc}) => :whatever :times 1))
