(ns todo-clojure.test.json
  (:use [todo-clojure.json])
  (:use [midje.sweet])
  (:require [todo-clojure.core :as core]))


(defmacro request [& [accept]]
  `(let [headers-map# (if (empty? '~accept) {} {"accept" '~accept})]
    (core/app (hash-map :request-method :get :uri "/" :headers headers-map#))))

(fact
  (request) => anything
  (provided (response-body-to-json anything) => :whatever :times 0))

(fact
  (request "*/*") => anything
  (provided (response-body-to-json anything) => :whatever :times 0))

(fact
  (request "application/json") => anything
  (provided (response-body-to-json anything) => :whatever :times 1))

(fact
  (request "application/json, text/javascript, */*; q=0.01") => anything
  (provided (response-body-to-json anything) => :whatever :times 1))

(fact "Should return a json version of the previous body"
	(:body (response-body-to-json {:body [1 2 3]})) => "[1,2,3]")

(let [id "4ec4431e0364a810ff60793e"]
 (fact "Can convert correctly a bson object id"
	(:body (response-body-to-json {:body (org.bson.types.ObjectId. id)})) => (str "\"" id "\"")))