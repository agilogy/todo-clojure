(ns todo-clojure.test.controllers
  (:use [todo-clojure.controllers])
  (:use [midje.sweet])
  (:require [todo-clojure.core :as core])
  (:require [todo-clojure.models :as models]))


(defmacro request [method uri & args]
  `(core/app (hash-map :request-method ~method :uri ~uri ~@args)))

;; Not found

(fact "An URI that does not exist should return a 404"
    (:status (request :get "/someinventeduri")) => 404)

;; GET /api/1/tasklist

(fact
  (request :get "/api/1/tasklist") => anything
  (provided (task-list) => :whatever :times 1))


(fact "Should return the task list from the model with _id transformed into id"
  (:body (task-list)) => [{:id "whatever" :task "Test the app"}]
  (provided
    (models/task-list) => [{:_id "whatever" :task "Test the app"}] :times 1))

;; GET /api/1/tasklist/:id/done

(fact
  (request :post "/api/1/tasklist/0/done") => anything
  (provided (task-completed "0") => :whatever :times 1 ))


(fact "Should invoke models/task-completed with the same id and return a redirect(303)"
  (:status (task-completed "0")) => 303
  (provided
    (models/task-completed "0") => anything :times 1))

;; POST /api/1/tasklist {:task description}

(fact
  (request :post "/api/1/tasklist" :params {"task" "Something"}) => anything
  (provided (models/add-task "Something") => :whatever :times 1))

(fact "Should invoke models/add-task with the same description and return a redirect(303)"
  (:status (add-task "Something")) => 303
  (provided
    (models/add-task "Something") => anything :times 1))