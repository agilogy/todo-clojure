(ns todo-clojure.test.config
  (:use [midje.sweet])
  (:use [todo-clojure.config]))

(def test-uri "mongodb://USER:PASSWORD@HOST:50/DB_NAME")

(fact
	(parse-uri test-uri) => {:username "USER" :password "PASSWORD" :host "HOST" 
							:port 50 :database "DB_NAME"})